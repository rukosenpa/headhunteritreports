import asyncio
import sys
from collections import Counter

import aiohttp
from openpyxl import Workbook, load_workbook

# Объявляем базовые настройки для работы
BASE_URL = "https://api.hh.ru/vacancies/"
REGION_ID = 54  # ID Краясноярска на HH.ru
SPECIALIZATION_ID = 1  # ID специализации Иниформационные технологии, телеком
PER_PAGE = 100  # Количество вакансий ответе
TARGET_JOB_CLUSTERS = (
    '1C',
    'SEO',
    'SMM',
    'WEB',
    'Мобильные',
    'Программирование',
    'Системный администратор',
    'Программист',
)

# Настройки HTTP заголовков для запросов
CLIENT_HEADERS = {
    'User-Agent': 'RukosenpasVacanciesParser/0.1'
}


# Эта функция занимается самой загрузкой данных о вакансиях
async def load_vacancies(cluster_name, session):
    # Задаем базовые параметры запроса
    query_parameters = {
        'area': REGION_ID,
        'only_with_salary': 'True',
        'specialization': SPECIALIZATION_ID,
        'per_page': PER_PAGE,
        'text': cluster_name,
    }

    # Т.к. HH ограничивает количество вакансий на запрос одной сотней,
    # то, в случаях когда вакансий больше ста, нужно будет делать несколько запросов к серверу.

    # Делаем запрос для того, чтобы чекнуть количество страниц в списке вакансий
    async with session.get(BASE_URL, params=query_parameters) as response:
        data = await response.json()
        # Целочисленное значение с количеством страниц
        pages = data['pages']

    # Список-контейнер для данных вакансий в который будем складывать результаты запроса
    vacancies = []

    # Т.к. в стандартном ответе сервера в данных о вакансии не содержатся все необходимые нам данные о вакансии,
    # то нужно будет для каждой вакансии делать отдельный запрос
    for page_num in range(pages):

        query_parameters['page'] = page_num
        # Делаем запрос со значением конкретной страницы списка вакансий
        async with session.get(BASE_URL, params=query_parameters) as list_vacancies_response:
            data = await list_vacancies_response.json()

        # В объекте вакансии, который мы получили, когда делали запрос
        # списка вакансий нужно достать URL вакансии,
        # сделать по нему запрос и сохранить необходимые данные в объект-контейнер(vacanies)
        for vacancy in data['items']:
            # Делаем запрос данных вакансии
            async with session.get(vacancy['url']) as detail_vacancy_response:
                vacancy_data = await detail_vacancy_response.json()
                # Добавляем в объект-контейнер
                vacancies.append(vacancy_data)

    return vacancies


async def process_cluster_data(cluster_name, session):
    print(f"Start process cluster: {cluster_name}")

    # Загружаем все данные о вакансиях
    data = await load_vacancies(cluster_name, session)

    # Создаем новый Excel файл
    workbook = Workbook()
    workbook.remove(workbook['Sheet'])  # Удаляем дефолтный лист
    vacancy_ws = workbook.create_sheet('Вакансии', 0)

    # Пишем заголовок "Всего вакансий" и количество вакансий
    vacancy_ws.cell(1, 1, 'Всего вакансий')
    vacancy_ws.cell(1, 2, len(data))

    # Пишем строку заголовков
    vacancy_ws.cell(3, 1, 'Название', )
    vacancy_ws.cell(3, 2, 'Описание')
    vacancy_ws.cell(3, 3, 'Зарплата')
    vacancy_ws.cell(3, 4, 'Название компании')
    vacancy_ws.cell(3, 5, 'Ссылка')

    # Контейнер для хранения значений ключевых навыков
    skills = []
    for i, vacancy in enumerate(data, start=4):
        # Пишем название вакансии в ячейку строки i, 1-го столбца
        vacancy_ws.cell(i, 1, vacancy['name'])
        # Пишем название вакансии в ячейку строки i, 2-го столбца
        vacancy_ws.cell(i, 2, vacancy['description'])

        # Вычисляем зарплату
        salary_from = vacancy['salary']['from'] or 0
        salary_to = vacancy['salary']['to'] or 0
        vacancy_ws.cell(
            i, 3,
            (salary_from + salary_to) / 2  # Calculate average value.
        )

        vacancy_ws.cell(i, 4, vacancy['employer']['name'])
        vacancy_ws.cell(i, 5, vacancy['alternate_url'])
        skills += [
            skill_obj['name'].lower()
            for skill_obj in vacancy['key_skills']
        ]

    # Counter — это подкласс dict, который используется для подсчета объектов hashable.
    # Элементы хранятся в качестве ключей словаря, а количество объектов сохранено в качестве значения
    # Таким образом мы подсчитываем количество требований конкретного ключевого навыка
    counted_skills = Counter(skills)

    # Создаем лист ключевых навыков
    skills_ws = workbook.create_sheet('Навыки', 1)
    skills_ws.cell(1, 1, 'Ключевые навыки')
    skills_ws.cell(1, 2, 'Частота')

    for i, skill in enumerate(
            sorted(
                counted_skills, key=lambda skill_: counted_skills[skill_], reverse=True
            ),
            start=2
    ):
        skills_ws.cell(i, 1, skill)
        skills_ws.cell(i, 2, counted_skills[skill])

    previous_data_workbook_filename = f'{cluster_name}_2009-2014.xlsx'
    try:
        # Пытаемся загрузить файл с предыдущими значениями ключевых навыков
        # Если файлов нет или они не найдены выбрасываем исключение
        previous_data_workbook = load_workbook(
            previous_data_workbook_filename, read_only=True)
        previous_data_worksheet = previous_data_workbook.active

        # Пишем заголовок годов
        for j in range(1, 7):
            skills_ws.cell(1, j + 4, previous_data_worksheet[1][j].value)
        skills_ws.cell(1, 11, 2021)

        # Для каждого навыка переписываем строку значений
        for i in range(2, previous_data_worksheet.max_row + 1):

            # Название навыка
            skill_name = previous_data_worksheet[i][0].value

            # Проблема в том, что в старых файлах значение в ячейке может не быть,
            # но если ячейка когда-либо была изменена, то она помечается измененной,
            # и пакет считает будто в ней что-то есть, даже если там ничего нет.
            # Поэтому нужно сделать проверку что skill_name не None.
            if skill_name:
                for j in range(7):
                    skills_ws.cell(
                        i, j + 4, previous_data_worksheet[i][j].value
                    )
                # Если навык есть в вакансиях,
                # которые мы загрузили, то мы берем значение, которое там есть
                if skill_name in counted_skills:
                    skills_ws.cell(
                        i, 11, counted_skills[skill_name]
                    )
    except FileNotFoundError:
        print(
            f'Файл {previous_data_workbook_filename} не найден. '
            f'Проверьте наличие файла в папке или его название'
        )
    # Сохранение Excel файла
    workbook.save(f'{cluster_name}.xlsx')
    print(f"{cluster_name} processed")


async def main():
    async with aiohttp.ClientSession(headers=CLIENT_HEADERS) as session:
        # Создание задач для параллельной обработки кластеров
        tasks = [process_cluster_data(cluster_name, session)
                 for cluster_name in TARGET_JOB_CLUSTERS]

        await asyncio.gather(*tasks, return_exceptions=True)
    print("Done")


if sys.version_info[0] == 3 and sys.version_info[1] >= 8 and sys.platform.startswith('win'):
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
asyncio.run(main(), debug=True)
