# HeadHunterITReports
## Description
Script for create reports for IT sector vacancies on hh.ru
## Requirements
- Python 3.7 and greater

## Installing
`pip install -r requirements.txt`

## Running
`python ./main.py`
